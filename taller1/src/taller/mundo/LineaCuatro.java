package taller.mundo;

import java.util.ArrayList;

public class LineaCuatro 
{
	/**
	 * Matriz que representa el estado actual del juego.
	 */
	private String[][] tablero;
	
	/**
	 * Lista que contiene a los jugadores
	 */
	private ArrayList<Jugador> jugadores;
	
	/**
	 * Nombre del jugador en turno
	 */
	private String atacante;
	
	/**
	 * Determina si el juego se terminó
	 */
	private boolean finJuego;
	
	/**
	 * El número del jugador en turno
	 */
	private int turno;
	
	private static final String POSICION_VACIA = "___"; 
	
	/**
	 * Crea una nueva instancia del juego
	 */
	public LineaCuatro(ArrayList <Jugador> pJugadores,int pFil, int pCol)
	{
		tablero = new String[pFil][pCol];
		for(int i=0;i<tablero.length;i++)
		{
			for(int j=0;j<tablero[0].length;j++)
			{
				tablero[i][j]=POSICION_VACIA;
			}

		}
		jugadores = pJugadores;
		finJuego = false;
		turno = 0;
		atacante = jugadores.get(turno).darNombre();
	}
	/**
	 * Retorna el tablero del juego
	 * @return tablero
	 */
	public String[][] darTablero()
	{
		return tablero;
	}
	
	/**
	 * Retorna el jugador en turno
	 * @return atacante jugador en turno
	 */
	public String darAtacante()
	{
		return atacante;
	}
	/**
	 * Determina si el juego termina
	 * @return true si el juego se termino, false de lo contrario
	 */
	public boolean fin()
	{
		
		return finJuego;
	}
	
	/**
	 * Registra una jugada aleatoria
	 */
	public void registrarJugadaAleatoria()
	{
		//TODO
		while(
				!registrarJugada((int)(Math.random()*tablero.length))
				);
		
	}
	
	/**
	 * Registra una jugada en el tablero
	 * @return true si la jugada se pudo realizar, false de lo contrario
	 */
	public boolean registrarJugada(int col)
	{
		if (col>tablero[0].length)
		{
			return false;
		}
		String[]columna=tablero[col];
		for (int i = 0; i < columna.length; i++) 
		{
			if (columna[i].equals(POSICION_VACIA)) 
			{
				columna[i]="_"+jugadores.get(turno).darSimbolo()+"_";
				if(terminar(i, col))
				{
				finJuego=true;
				}
				else{
				turno++;
				if(turno>=jugadores.size())
				{
					turno=0;
				}
				atacante = jugadores.get(turno).darNombre();
				}
				return true;
				
				
			}
		}
		//TODO
		return false;
	}
	
	/**
	 * Determina si una jugada causa el fin del juego
	 * @return true si la jugada termina el juego false de lo contrario
	 */
	public boolean terminar(int fil,int col)
	{
		//TODO
		Jugador n=jugadores.get(turno);
		n.darSimbolo();
		int contadorSim=0;
		
		//caso horizontal
		String[] columna=tablero[col];
		System.out.println("valor col:"+columna);
		for (int i = fil; i <columna.length-1  ; i++) 
		{
			if(columna[i].equals(columna[i+1]))
			{
				contadorSim++;
			}
			else
			{
				break;
			}
				
		}
		for (int i = fil; i >0; i--)
		{
			if(columna[i].equals(columna[i-1]))
			{
				contadorSim++;
			}
			else{
				break;
			}
		}
		
		System.out.println("contador consecutivo"+contadorSim);
		if(contadorSim+1>=4)
		{
			return true;
		}
		//caso horizontal
		int contadorH=0;
		for (int i = col; i < tablero.length-1; i++)
		{
			if (tablero[i][fil].equals(tablero[i+1][fil]))
			{
				contadorH++;
			}	
			else{
				break;
			}
			
		}
		for (int i = col; i > 0; i--)
		{
			if (tablero[i][fil].equals(tablero[i-1][fil]))
			{
				contadorH++;
			}
			else {
				break;
			}
		}
		if (contadorH+1>=4)
		{
			return true;
		}
		//conteo diagonal
		int contDiag=0;
		for (int i = col, j=fil; i < tablero.length-1 && j < tablero[i].length-1; i++, j++)
		{
			if(tablero[i][j].equals(tablero[i+1][j+1]))
				{
					contDiag++;
				}
			else {
				break;
			}
			
		}
		for (int i = col,j=fil ; i >0 && j>0; i--, j--)
		{
			if (tablero[i][j].equals(tablero[i-1][j-1]))
			{
              contDiag++;
			}
			else {
				break;
			}
		}
		if (contDiag+1>=4)
		{
		return true;	
		}
		contDiag=0;
		for (int i = col, j=fil; i < tablero.length-1 && j>0; i++, j--)
		{
			if(tablero[i][j].equals(tablero[i+1][j-1]))
			{
              contDiag++;
			}
			else{
				break;
			}
		}
		for (int i = col, j=fil; i>0 && j <tablero[i].length-1; i--, j++)
		{
			if (tablero[i][j].equals(tablero[i-1][j+1]))
			{
				contDiag++;
			}
			else {
				break;
			}
		}
		if(contDiag+1>=4)
		{
			return true;
		}
		return false;
		
		
	}



}
