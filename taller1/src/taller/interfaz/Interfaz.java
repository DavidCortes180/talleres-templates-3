package taller.interfaz;

import java.util.ArrayList;
import java.util.Scanner;

import taller.mundo.Jugador;
import taller.mundo.LineaCuatro;

public class Interfaz 
{
	/**
	 * Juego actual
	 */
	private LineaCuatro juego;
	
	/**
	 * Escáner de texto ingresado por el usuario en la consola
	 */
	private Scanner sc;
	
	/**
	 * Crea una nueva instancia de la clase de interacción del usuario con la consola
	 */
	public Interfaz()
	{
		sc= new Scanner (System.in);
		while (true)
		{
			imprimirMenu();
			try
			{
				int opt=Integer.parseInt(sc.next());
				if(opt==1)
				{
					empezarJuego();
				}
				else if(opt==2)
				{
					empezarJuegoMaquina();
				}
				else if(opt==3)
				{
					System.out.println("¡Vuelva pronto!");
					break;
				}
				else
				{
					System.out.println("Comando inválido");
					continue;
				}
			}
			catch (NumberFormatException e)
			{
				System.out.println("Comando inválido");
				continue;
			}


		}
	}
	/**
	 * Imprime el menú principal
	 */
	public void imprimirMenu()
	{
		System.out.println("------------------------LÍNEA CUATRO------------------------");
		System.out.println("-Menú principal-");
		System.out.println("Ingrese un comando:");
		System.out.println("1. Empezar juego con amigos");
		System.out.println("2. Empezar juego vs máquina");
		System.out.println("3. Salir");		
    }
	
	/**
	 * Crea un nuevo juego
	 */
	public void empezarJuego()
	{
		int filas=pedirFilas();
		int colum= pedirColumnas();
		
		System.out.println("Ingrese el numero de jugadores: ");
		
		int nJugadores;
		nJugadores=sc.nextInt();
		

		ArrayList<Jugador> jugadores= new ArrayList<>();
		
		for (int i = 0; i < nJugadores; i++) 
		{
			jugadores.add(registrarJugador());	
		}
		
		
		juego(jugadores,filas, colum);

       //TODO
	}
	
	public int pedirFilas ()
	{
		int filas=0;
		
		while (filas<4) 
		{
			System.out.println("Ingrese filas:");
			filas =nextInt();
			
			if (filas<4)
			{
				System.out.println("Tienen que ser minimo 4 filas");
			}
		}

		return filas;
	}
	public int nextInt()
	{

		boolean termino=false;
		int valor=-1;
		while(!termino)
		{
			try 
			{
				valor=sc.nextInt();
				termino=true;
			} 
			catch (Exception e)
			{
				// TODO: handle exception
				System.out.print("\ndebe ingresar un numero. Intente de nuevo: ");
				sc.nextLine();
			}
		}
		return valor;
	}

	public int  pedirColumnas() 
	{
		int columnas=0;
		
		while (columnas<4)
		{
			System.out.println("Ingrese columnas:");
			columnas =nextInt();
			if(columnas<4)
			{
		System.out.println("Deben ser minimo 4");
			}
			
		}
		return columnas;
	}
	
	public Jugador registrarJugador()
	{
		String nombre;
		String simbolo=null;
		
		System.out.println("Ingrese su nombre: ");
		nombre=sc.next();
		boolean termino=false;
		while(!termino)
		{
		System.out.println("Ingrese su Simbolo: ");
		simbolo=sc.next();
		if(simbolo.length()==1)
		{
			termino=true;
			
		}else
		{
			System.out.println("El simbolo solo puede ser de un caracter ");	
		}
		
		}
		
		return new Jugador(nombre,simbolo);
	}
	/**
	 * Modera el juego entre jugadores
	 */
	public void juego(ArrayList <Jugador> pJugadores,int pFil, int pCol)
	{
		//TODO
		juego=new LineaCuatro(pJugadores, pFil, pCol);
		while (!juego.fin())
		{
			imprimirTablero();
			String jugador=juego.darAtacante();
			System.out.println(jugador +  " Ingrese la columna en la que quiere jugar");
			int columnaAJugar;
			
			boolean sePudoJugar=false;
			while (!sePudoJugar)
			{
				columnaAJugar=nextInt();
				sePudoJugar=juego.registrarJugada(columnaAJugar);
				if (!sePudoJugar)
				{
					System.out.println("no se pudo jugar.Intente otra columna");
				}
				
			}
			
		}
		System.out.println("el juego se acabo, el ganador es: "+juego.darAtacante());
	}
	/**
	 * Empieza el juego contra la máquina
	 */
	public void empezarJuegoMaquina()
	{
		int filas=pedirFilas();
		int colum= pedirColumnas();
		
		ArrayList<Jugador> jugadores= new ArrayList<>();
		jugadores.add(registrarJugador());
		jugadores.add(new Jugador("computador", "X"));
		
		juegoMaquina(jugadores,filas, colum);
		//TODO
	}
	/**
	 * Modera el juego contra la máquina
	 */
	public void juegoMaquina(ArrayList <Jugador> pJugadores,int pFil, int pCol)
	{
		//TODO
		juego=new LineaCuatro(pJugadores, pFil, pCol);
		while (!juego.fin())
		{
			imprimirTablero();
			String jugador=juego.darAtacante();
			if(jugador.equals("computador"))
				{
				juego.registrarJugadaAleatoria();
				}
			else
			{
				int columnaAJugar;
				boolean sePudoJugar=false;
				while (!sePudoJugar)
				{
					System.out.println(jugador +  " Ingrese la columna en la que quiere jugar");
					columnaAJugar=nextInt();
					sePudoJugar=juego.registrarJugada(columnaAJugar);
					if (!sePudoJugar)
					{
						System.out.println("no se pudo jugar.Intente otra columna");
					}
					
				}
			}
				
			
		}
	System.out.println("Ha ganado el jugador: "+juego.darAtacante());
		//TODO
	}

	/**
	 * Imprime el estado actual del juego
	 */
	public void imprimirTablero()
	{
		//TODO
		System.out.println("-------------------");
		String[][]tablero=juego.darTablero();
		for (int j = tablero[0].length-1; j >=0; j--) 
		{
			for (int i = 0; i < tablero.length; i++) 
			{
				System.out.print(tablero[i][j] + "|");
			}
			System.out.println();
		}
		
		for (int i = 0; i < tablero.length; i++) {
			System.out.print(" "+i+" |");		
		}
		System.out.println("\n-------------------");
	}
}
